package com.eljacko.survey.datamodel;

import com.eljacko.survey.datamodel.entity.Sector;
import com.eljacko.survey.datamodel.utils.ForeignEntityBuilder;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Consumer;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
@SuppressWarnings({ "checkstyle:magicnumber" })
public abstract class AbstractIntegrationTest {
    public abstract TestEntityManager getEntityManager();

    protected final Long getRunId() {
        return new Long(new SimpleDateFormat("yyyMMddHHmmssS").format(new Date()));
    }

    protected final void flushAndClear() {
        getEntityManager().flush();
        getEntityManager().clear();
    }

    protected final Sector getInsertedSector(final Consumer<Sector> dataSetter) {
        final Sector sectorDb = getEntityManager()
                .persistFlushFind(ForeignEntityBuilder.getSector(null, dataSetter));
        getEntityManager().flush();
        return sectorDb;
    }

    protected final Sector saveAndReturnSector(@NotNull final Sector category,
                                             final Consumer<Sector> sectorDataSetter) {
        Sector retSector = ForeignEntityBuilder.getSectorWithCategory(category, sectorDataSetter);
        retSector = getEntityManager().persistAndFlush(retSector);
        return retSector;
    }

}
