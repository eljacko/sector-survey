package com.eljacko.survey.datamodel.repository;

import com.eljacko.survey.datamodel.AbstractIntegrationTest;
import com.eljacko.survey.datamodel.constant.FieldsLength;
import com.eljacko.survey.datamodel.dto.SectorDto;
import com.eljacko.survey.datamodel.entity.Sector;
import com.eljacko.survey.datamodel.entity.utils.StringTestUtil;
import com.eljacko.survey.datamodel.utils.ForeignEntityBuilder;
import lombok.Getter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import java.util.List;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.beans.SamePropertyValuesAs.samePropertyValuesAs;
import static org.hamcrest.MatcherAssert.assertThat;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SuppressWarnings({ "checkstyle:MethodName", "checkstyle:MagicNumber" })
public class SectorRepositoryTest extends AbstractIntegrationTest {
    @Autowired
    private SectorRepository repository;
    @Getter
    @Autowired
    private TestEntityManager entityManager;

    private transient Sector globalSector;

    private void saveSector() {
        globalSector = ForeignEntityBuilder.getSector();
        repository.save(globalSector);
        entityManager.flush();
    }

    /*
     * JpaRepository method tests
     */

    @Test
    public final void findById_returnOne() {
        saveSector();
        Optional<Sector> actual = repository.findById(globalSector.getId());
        assertThat(actual.get(), samePropertyValuesAs(globalSector));
    }

    @Test
    public final void findById_returnNull() {
        Optional<Sector> fromDb = repository.findById(getRunId());
        assertThat(fromDb.isPresent(), is(false));
    }

    @Test
    public final void getSectorsTree_OneSectorWithOtherAsCategory_ListOfOne() {
        Sector category = getInsertedSector(null);
        saveAndReturnSector(category, null);

        List<SectorDto> fromDb = repository.getSectorsTree(null, null);
        flushAndClear();

        assertThat(fromDb.size(), is(2));
    }

    @Test
    public final void getSectorsTree_MultipleSectorsMultipleCategories_MultipleResults() {
        Sector category1 = getInsertedSector(null);
        Sector category2 = getInsertedSector(null);
        saveAndReturnSector(category1, null);
        saveAndReturnSector(category1, null);
        saveAndReturnSector(category2, null);

        List<SectorDto> fromDb = repository.getSectorsTree(null, null);
        flushAndClear();

        assertThat(fromDb.size(), is(5));
    }

    /*
     * Entity tests
     */

    @Test
    public final void save_requiredData() {
        Sector expected = ForeignEntityBuilder.getSector();
        repository.save(expected);
        flushAndClear();

        Sector savedSector = entityManager.find(Sector.class, expected.getId());
        assertThat(savedSector.getName(), is(expected.getName()));
    }

    @Test
    public final void save_withMaxLength() {
        Sector expected = ForeignEntityBuilder.getSector(StringTestUtil
                .randomStringWithLength(FieldsLength.SECTOR_NAME), null);
        Sector actual = repository.save(expected);
        entityManager.flush();

        assertThat(expected, samePropertyValuesAs(actual));
    }

    @Test
    public final void save_withCategory() {
        Sector category = ForeignEntityBuilder.getSector(null, null);
        Sector expected = ForeignEntityBuilder.getSector(null, e -> e.setCategory(category));
        Sector actual = repository.save(expected);
        entityManager.flush();

        assertThat(expected, samePropertyValuesAs(actual));
    }
}
