package com.eljacko.survey.datamodel.utils;

import com.eljacko.survey.datamodel.entity.Sector;
import com.eljacko.survey.datamodel.entity.User;
import com.eljacko.survey.datamodel.entity.utils.StringTestUtil;
import com.eljacko.survey.datamodel.entity.utils.UnitTestHelper;
import org.springframework.util.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.function.Consumer;

@SuppressWarnings({ "checkstyle:magicnumber" })
public final class ForeignEntityBuilder {
 // @formatter:off
    public static final String ALPHABET =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
 // @formatter:on
    public static Long getRunId() {
        return new Long(new SimpleDateFormat("yyyMMddHHmmssS").format(new Date()));
    }

    public static Sector getSector() {
        return getSector(null, null);
    }

    public static Sector getSector(final String name, final Consumer<Sector> dataSetter) {
        final Sector sector = new Sector();
        if (StringUtils.isEmpty(name)) {
            sector.setName(StringTestUtil.randomStringWithLength(10));
        } else {
            sector.setName(name);
        }
        if (dataSetter != null) {
            dataSetter.accept(sector);
        }
        return sector;
    }

    public static Sector getSectorWithCategory(final Sector category,
                                               final Consumer<Sector> dataSetter) {
        final Sector sector = getSector(null, dataSetter);
        sector.setCategory(category);
        if (dataSetter != null) {
            dataSetter.accept(sector);
        }
        return sector;
    }

    public static User getUser(final String name, final Consumer<User> dataSetter) {
        final User user = new User();
        if (StringUtils.isEmpty(name)) {
            user.setName(StringTestUtil.randomStringWithLength(10));
        } else {
            user.setName(name);
        }
        if (dataSetter != null) {
            dataSetter.accept(user);
        }
        return user;
    }

    public static User getUser() {
        final User user = getUser(null, null);
        user.setLastModified(UnitTestHelper.currentTimeAsTimestamp());
        user.setTermsAccepted(UnitTestHelper.currentTimeAsTimestamp());
        return user;
    }

    private ForeignEntityBuilder() {
        throw new UnsupportedOperationException(
                "This is a utils class and cannot be instantiated");
    }
}
