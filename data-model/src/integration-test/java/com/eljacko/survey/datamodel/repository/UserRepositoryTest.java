package com.eljacko.survey.datamodel.repository;

import com.eljacko.survey.datamodel.AbstractIntegrationTest;
import com.eljacko.survey.datamodel.entity.Sector;
import com.eljacko.survey.datamodel.entity.User;
import com.eljacko.survey.datamodel.utils.ForeignEntityBuilder;
import lombok.Getter;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@SuppressWarnings({ "checkstyle:MethodName" })
public class UserRepositoryTest extends AbstractIntegrationTest {
    @Autowired
    private UserRepository repository;
    @Getter
    @Autowired
    private TestEntityManager entityManager;

    /*
     * Entity tests
     */

    @Test
    public final void save_requiredData() {
        User expected = ForeignEntityBuilder.getUser();
        repository.save(expected);
        flushAndClear();

        Sector savedSector = entityManager.find(Sector.class, expected.getId());
        assertThat(savedSector.getName(), is(expected.getName()));
    }

}
