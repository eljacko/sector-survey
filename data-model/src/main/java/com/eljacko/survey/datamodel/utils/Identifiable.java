package com.eljacko.survey.datamodel.utils;

import java.io.Serializable;

public interface Identifiable<T extends Serializable> {
    T getId();
}
