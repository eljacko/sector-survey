package com.eljacko.survey.datamodel.entity;

import com.eljacko.survey.datamodel.entity.base.BaseEntity;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 * Enabled users for sector
 */
@Entity
@Table(name = "user_sector")
@DynamicUpdate
@DynamicInsert
@Setter
@ToString(callSuper = true)
@NoArgsConstructor
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.survey.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "user_sector_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class UserSector extends BaseEntity {
    private static final long serialVersionUID = 1L;

    private User user;
    private Sector sector;

    public UserSector(final User user, final Sector sector) {
        super();
        this.user = user;
        this.sector = sector;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false, updatable = false)
    public User getUser() {
        return user;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "sector_id", nullable = false, updatable = false)
    public Sector getSector() {
        return sector;
    }
}
