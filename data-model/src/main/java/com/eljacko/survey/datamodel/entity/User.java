package com.eljacko.survey.datamodel.entity;

import com.eljacko.survey.datamodel.constant.ValidationMessages;
import com.eljacko.survey.datamodel.entity.base.BaseEntity;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.Set;

@Entity
@Table(name = "users")
@ToString(callSuper = true)
@Setter
@SuppressWarnings({ "checkstyle:DesignForExtension" })
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.survey.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "users_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
public class User extends BaseEntity {
    private static final long serialVersionUID = 8098043870298083510L;

    private String name;
    private String token;
    // Filled when the parcel is returned, used as a boolean
    // Timestamp used instead of boolean as performance isn't as important in here and
    // timestamp offers more value later, when needed
    private Timestamp lastModified;
    private Timestamp termsAccepted;
    private Set<UserSector> userSectors;

    @NotNull(message = ValidationMessages.NOT_NULL)
    @Column(name = "name", nullable = false)
    public String getName() {
        return name;
    }

    @Column(name = "token")
    public String getToken() {
        return token;
    }

    @NotNull(message = ValidationMessages.NOT_NULL)
    @Column(name = "terms_accepted", nullable = false)
    public Timestamp getTermsAccepted() {
        return termsAccepted;
    }

    @NotNull(message = ValidationMessages.NOT_NULL)
    @Column(name = "last_modified", nullable = false)
    public Timestamp getLastModified() {
        return lastModified;
    }

    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = CascadeType.ALL,
            orphanRemoval = true)
    public Set<UserSector> getUserSectors() {
        return userSectors;
    }

}
