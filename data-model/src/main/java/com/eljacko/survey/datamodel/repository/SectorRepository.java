package com.eljacko.survey.datamodel.repository;

import com.eljacko.survey.datamodel.dto.SectorDto;
import com.eljacko.survey.datamodel.entity.Sector;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface SectorRepository extends JpaRepository<Sector, Long> {

    /**
     * Get sector by id.
     *
     * @return optional sector
     */
    Optional<Sector> findById(Long id);

    /**
     * Get all sectors from top category to sectors by level
     *
     * @return set of sectors
     */
    @Query(nativeQuery = true)
    List<SectorDto> getSectorsTree(@Param("categoryId") Long categoryId,
                                   @Param("depth") Integer depth);

}
