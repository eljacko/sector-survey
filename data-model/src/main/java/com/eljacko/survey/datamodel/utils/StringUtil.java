package com.eljacko.survey.datamodel.utils;

public final class StringUtil {

    public static String parseString(final String term) {
        return "%" + term.toLowerCase().trim() + "%";
    }

    private StringUtil() {
        throw new UnsupportedOperationException(
                "This is a utility class and cannot be instantiated");
    }

}
