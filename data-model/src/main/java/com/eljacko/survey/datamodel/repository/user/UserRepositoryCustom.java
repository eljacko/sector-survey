package com.eljacko.survey.datamodel.repository.user;

import com.eljacko.survey.datamodel.dto.UserDto;

import javax.validation.constraints.NotNull;

public interface UserRepositoryCustom {

    UserDto getUser(@NotNull Long userId);

}
