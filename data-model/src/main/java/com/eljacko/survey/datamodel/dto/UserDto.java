package com.eljacko.survey.datamodel.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Slf4j
public class UserDto {

    private Long id;
    private String name;
    private Boolean termsAccepted;
    private List<UserSectorDto> sectors;

    public UserDto() {
    }

}
