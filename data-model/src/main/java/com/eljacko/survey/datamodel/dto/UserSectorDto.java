package com.eljacko.survey.datamodel.dto;

import com.eljacko.survey.datamodel.entity.UserSector;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class UserSectorDto {

    private Long sectorId;
    private String sectorName;
    private Long userSectorId;

    public UserSectorDto(final Long sectorId) {
        super();
        this.sectorId = sectorId;
    }

    public UserSectorDto(final UserSector userSector) {
        super();
        this.sectorId = userSector.getSector().getId();
    }
}
