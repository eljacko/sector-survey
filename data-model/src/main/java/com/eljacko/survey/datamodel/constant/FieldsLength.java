package com.eljacko.survey.datamodel.constant;

public final class FieldsLength {

    public static final int SECTOR_NAME = 64;

    private FieldsLength() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
