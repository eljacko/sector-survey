package com.eljacko.survey.datamodel.repository.user;

import com.eljacko.survey.datamodel.dto.UserDto;
import com.eljacko.survey.datamodel.dto.UserSectorDto;
import com.eljacko.survey.datamodel.entity.Sector;
import com.eljacko.survey.datamodel.entity.Sector_;
import com.eljacko.survey.datamodel.entity.User;
import com.eljacko.survey.datamodel.entity.UserSector;
import com.eljacko.survey.datamodel.entity.UserSector_;
import com.eljacko.survey.datamodel.entity.User_;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.Assert;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Tuple;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Slf4j
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class UserRepositoryCustomImpl implements UserRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public UserDto getUser(final Long userId) {
        Assert.notNull(userId, "userId must not be null");

        TypedQuery<Tuple> q = getQuery(userId);
        List<Tuple> tupleResult = q.getResultList();

        UserDto result = new UserDto();
        result.setSectors(new ArrayList<>());
        boolean setMainData = true;
        for (Tuple t : tupleResult) {
            if (setMainData) {
                result.setId((Long) t.get("userId"));
                result.setName((String) t.get("userName"));
                Timestamp termsAccepted = (Timestamp) t.get("termsAccepted");
                result.setTermsAccepted(termsAccepted != null);
                setMainData = false;
            }
            UserSectorDto userSectorDto = new UserSectorDto();
            userSectorDto.setSectorId((Long) t.get("sectorId"));
            userSectorDto.setUserSectorId((Long) t.get("userSectorId"));
            userSectorDto.setSectorName((String) t.get("sectorName"));
            result.getSectors().add(userSectorDto);
        }

        return result;
    }

    @SuppressWarnings({"unchecked", "checkstyle:MethodLength"})
    private TypedQuery<Tuple> getQuery(final Long userId) {
        CriteriaBuilder cb = getEm().getCriteriaBuilder();
        CriteriaQuery<Tuple> criteriaQuery = cb.createQuery(Tuple.class);
        Root<User> userTable = criteriaQuery.from(User.class);
        Join<User, UserSector> userUserSectorJoin = userTable
                .join(User_.userSectors, JoinType.INNER);
        Join<UserSector, Sector> userSectorSectorJoin = userUserSectorJoin
                .join(UserSector_.sector, JoinType.INNER);

        criteriaQuery.multiselect(
                userTable.get(User_.id).alias("userId"),
                userTable.get(User_.name).alias("userName"),
                userTable.get(User_.termsAccepted).alias("termsAccepted"),
                userUserSectorJoin.get(UserSector_.id).alias("userSectorId"),
                userSectorSectorJoin.get(Sector_.id).alias("sectorId"),
                userSectorSectorJoin.get(Sector_.name).alias("sectorName")
        );

        List<Predicate> wherePredicateList = new ArrayList<>();
        wherePredicateList.add(cb.equal(userTable.get(User_.id),
                cb.parameter(Long.class, "userId")));

        criteriaQuery.where(
                cb.and(wherePredicateList.toArray(new Predicate[wherePredicateList.size()])));

        TypedQuery<Tuple> q = getEm().createQuery(criteriaQuery);
        q.setParameter("userId", userId);
        return q;
    }
}
