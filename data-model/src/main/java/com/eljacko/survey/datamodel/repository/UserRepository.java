package com.eljacko.survey.datamodel.repository;

import com.eljacko.survey.datamodel.entity.User;
import com.eljacko.survey.datamodel.repository.user.UserRepositoryCustom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom {

    Optional<User> findById(Long id);

    @Query("SELECT (COUNT(u) > 0) "
            + "FROM User u "
            + "WHERE u.id = :userId "
            + "AND u.token = :token "
            + "AND u.lastModified >= :oldestAllowed")
    Boolean isUpdateAllowed(@Param("userId") Long userId,
                            @Param("token") String token,
                            @Param("oldestAllowed") Timestamp oldestAllowed);


}
