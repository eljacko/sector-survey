package com.eljacko.survey.datamodel.constant;

public final class DateFormats {
    public static final String RFC3339 = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
    public static final String ISO_DATE_TIME = "yyyy-MM-dd'T'HH:mm:ss'Z'";

    private DateFormats() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
