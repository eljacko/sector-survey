package com.eljacko.survey.datamodel.repository;

import com.eljacko.survey.datamodel.entity.UserSector;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserSectorRepository extends JpaRepository<UserSector, Long> {

}
