package com.eljacko.survey.datamodel.dto;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@Getter
@Setter
@ToString
@EqualsAndHashCode
public class SectorDto {

    private Long id;
    private String name;
    private Long categoryId;
    private Integer level;
    private List<SectorDto> sectors;

    public SectorDto() {
    }

    public SectorDto(final Long id, final String name, final Long categoryId, final Integer level) {
        super();
        this.id = id;
        this.name = name;
        this.categoryId = categoryId;
        this.level = level;
    }

}
