package com.eljacko.survey.datamodel.entity;

import com.eljacko.survey.datamodel.constant.FieldsLength;
import com.eljacko.survey.datamodel.constant.ValidationMessages;
import com.eljacko.survey.datamodel.dto.SectorDto;
import com.eljacko.survey.datamodel.entity.base.BaseEntity;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.ColumnResult;
import javax.persistence.ConstructorResult;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedNativeQueries;
import javax.persistence.NamedNativeQuery;
import javax.persistence.OneToMany;
import javax.persistence.SqlResultSetMapping;
import javax.persistence.SqlResultSetMappings;
import javax.persistence.Table;
import javax.validation.constraints.Size;
import java.util.Set;

// @formatter:off
@NamedNativeQueries({
    @NamedNativeQuery(name = "Sector.getSectorsTree", query =
        "WITH RECURSIVE cte (id, name, category_id, level) AS ( "
        + " SELECT t.id, t.name, t.category_id, 0 AS level "
        + " FROM public.sectors AS t "
        + " WHERE (t.category_id IS NULL "
        // not prettiest, but avoids null being transformed into bytea
        // caused by sql having numerous types of null
        + " OR t.category_id = CAST(CAST(:categoryId AS TEXT) AS BIGINT))"
        + " UNION ALL "
        + " SELECT t1.id, t1.name, t1.category_id, t2.level + 1  "
        + " FROM public.sectors AS t1 "
        + " INNER JOIN cte t2 ON t1.category_id = t2.id "
        + " WHERE t1.category_id IS NOT NULL "
        + " AND (level < CAST(CAST(:depth AS TEXT) AS INTEGER) OR :depth IS NULL) "
        + " ) SELECT * FROM cte "
        + " ORDER BY (level, category_id) ",
        resultSetMapping = "SectorDtoMapping")
})
// @formatter:on
@SqlResultSetMappings({
        @SqlResultSetMapping(name = "SectorDtoMapping", classes = {
                @ConstructorResult(targetClass = SectorDto.class, columns = {
                        @ColumnResult(name = "id", type = Long.class),
                        @ColumnResult(name = "name", type = String.class),
                        @ColumnResult(name = "category_id", type = Long.class),
                        @ColumnResult(name = "level", type = Integer.class)
                }) }),
})
@Entity
@Table(name = "sectors")
@ToString(callSuper = true)
@Setter
@SuppressWarnings({ "checkstyle:DesignForExtension" })
//@formatter:off
@GenericGenerator(
        name = "assigned-sequence",
        strategy = "com.eljacko.survey.datamodel.utils.AssignedSequenceStyleGenerator",
        parameters = {
                @org.hibernate.annotations.Parameter(
                        name = "sequence_name",
                        value = "sectors_id_seq"),
                @org.hibernate.annotations.Parameter(
                        name = "optimizer",
                        value = "none")
        })
//@formatter:on
public class Sector extends BaseEntity {
    private static final long serialVersionUID = 1549505726733603361L;

    private String name;
    private Sector category;
    @ToString.Exclude
    private Set<Sector> items;

    public Sector() {
        super();
    }

    @Size(max = FieldsLength.SECTOR_NAME, message = ValidationMessages.MAX_LENGTH_EXCEEDED)
    @Column(length = FieldsLength.SECTOR_NAME, nullable = false)
    public String getName() {
        return name;
    }

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    public Sector getCategory() {
        return category;
    };

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "id")
    public Set<Sector> getItems() {
        return items;
    };

}
