package com.eljacko.survey.datamodel.entity;

import com.eljacko.survey.datamodel.entity.base.BaseEntity;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.HashSet;

import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItems;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings({ "checkstyle:MethodName" })
public class BaseEntityUnitTest {

    @Test
    public final void equals_diffCode_Equal() {
        Sector entity1 = new Sector();
        entity1.setId(1L);

        BaseEntity entity2 = new Sector();
        entity2.setId(entity1.getId());

        assertThat(entity1, is(equalTo(entity2)));
    }

    @Test
    public final void equals_diffId_NotEqual() {
        BaseEntity entity1 = new Sector();
        entity1.setId(1L);

        BaseEntity entity2 = new Sector();
        entity2.setId(2L);

        assertThat(entity1, not(equalTo(entity2)));
    }

    @Test
    public final void equals_diffIdNull_NotEqual() {
        BaseEntity entity1 = new Sector();
        entity1.setId(1L);

        BaseEntity entity2 = new Sector();

        assertThat(entity1, not(equalTo(entity2)));
    }

    @Test
    public final void equals_nullIds_NotEqual() {
        BaseEntity entity1 = new Sector();

        BaseEntity entity2 = new Sector();

        assertThat(entity1, not(equalTo(entity2)));
    }

    @Test
    public final void hashCode_sameId() {
        BaseEntity entity1 = new Sector();
        entity1.setId(1L);

        BaseEntity entity2 = new Sector();
        entity2.setId(entity1.getId());
        HashSet<BaseEntity> ccsendSet = new HashSet<>(2);
        ccsendSet.add(entity1);
        ccsendSet.add(entity2);

        assertThat(ccsendSet, hasSize(1));
    }

    @Test
    public final void hashCode_differentIds() {
        BaseEntity entity1 = new Sector();
        entity1.setId(1L);

        BaseEntity entity2 = new Sector();
        entity2.setId(2L);
        HashSet<BaseEntity> ccsendSet = new HashSet<>(2);
        ccsendSet.add(entity1);
        ccsendSet.add(entity2);

        assertThat(ccsendSet, hasSize(2));
        assertThat(ccsendSet, hasItems(entity1, entity2));
    }

    @Test
    public final void hashCode_differentIdAndNull() {
        BaseEntity entity1 = new Sector();

        BaseEntity entity2 = new Sector();
        entity2.setId(2L);
        HashSet<BaseEntity> ccsendSet = new HashSet<>(2);
        ccsendSet.add(entity1);
        ccsendSet.add(entity2);

        assertThat(ccsendSet, hasSize(2));
        assertThat(ccsendSet, hasItems(entity1, entity2));
    }

    @Test
    public final void hashCode_nullIds() {
        BaseEntity entity1 = new Sector();
        BaseEntity entity2 = new Sector();

        HashSet<BaseEntity> ccsendSet = new HashSet<>(2);
        ccsendSet.add(entity1);
        ccsendSet.add(entity2);

        assertThat(ccsendSet, hasSize(2));
        assertThat(ccsendSet, hasItems(entity1, entity2));
    }

    @Test
    public final void hashCode_nullIds2() {
        BaseEntity entity1 = new Sector();
        BaseEntity entity2 = new Sector();

        HashMap<BaseEntity, BaseEntity> ccsendSet = new HashMap<>(2);
        ccsendSet.put(entity1, entity1);
        ccsendSet.put(entity2, entity2);

        assertThat(ccsendSet.size(), is(equalTo(2)));
    }
}
