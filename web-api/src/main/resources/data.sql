INSERT INTO public.sectors ("id", "name") VALUES (1, 'Manufacturing') ON CONFLICT DO NOTHING;
INSERT INTO public.sectors ("id", "name", "category_id") VALUES (2, 'Construction materials', 1) ON CONFLICT DO NOTHING;
INSERT INTO public.sectors ("id", "name", "category_id") VALUES (3, 'Electronics and Optics', 1) ON CONFLICT DO NOTHING;
INSERT INTO public.sectors ("id", "name", "category_id") VALUES (4, 'Food and Beverage', 1) ON CONFLICT DO NOTHING;
INSERT INTO public.sectors ("id", "name", "category_id") VALUES (5, 'Bakery &amp; confectionery products', 4) ON CONFLICT DO NOTHING;



