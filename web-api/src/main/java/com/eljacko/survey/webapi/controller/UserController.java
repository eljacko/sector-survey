package com.eljacko.survey.webapi.controller;

import com.eljacko.survey.webapi.constant.WebApiPaths;
import com.eljacko.survey.webapi.dto.FieldError;
import com.eljacko.survey.webapi.dto.response.SimpleTokenResponse;
import com.eljacko.survey.webapi.dto.user.UserData;
import com.eljacko.survey.webapi.dto.user.UserDataSaveResult;
import com.eljacko.survey.webapi.exception.InvalidParametersException;
import com.eljacko.survey.webapi.service.UserService;
import com.eljacko.survey.webapi.service.util.DateUtilService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/v1")
@SuppressWarnings("checkstyle:designforextension")
public class UserController {

    private final UserService userService;
    private final DateUtilService dateUtilService;

    @PostMapping(WebApiPaths.USERS)
    public HttpEntity<?> add(@RequestBody final UserData userData) {
        log.debug("Request to add user {}", userData);
        UserDataSaveResult result = null;
        if (isValidUserData(userData)) {
             result = save(null, userData, null);
        }
        return new ResponseEntity<UserDataSaveResult>(result, HttpStatus.OK);
    }

    @PutMapping(WebApiPaths.USER)
    final HttpEntity<?> update(
            @PathVariable("userId") final Long userId,
            @RequestBody @Valid final UserData userData,
            @RequestParam(value = "token", required = false) final String token) {
        log.debug("Request to update user: userId:{}, data={}", userId, userData);
        save(userId, userData, token);
        SimpleTokenResponse response = new SimpleTokenResponse();
        response.setTimestampAsStr(dateUtilService.getInstantNow());
        log.debug("User data update is done: userId:{}", userId);
        return new ResponseEntity<SimpleTokenResponse>(response, HttpStatus.OK);
    }

    @GetMapping(WebApiPaths.USER)
    public HttpEntity<?> get(@PathVariable("userId") final Long userId) {
        log.debug("Request to get user id:{}", userId);
        UserData userData = userService.getUserData(userId);
        log.debug("Response for getting user id:{}", userId);
        return new ResponseEntity<UserData>(userData, HttpStatus.OK);
    }

    private UserDataSaveResult save(final Long userId, final UserData userData,
                                    final String token) {
        UserDataSaveResult result = null;
        userData.setId(userId);
        if (isValidUserData(userData)) {
            result = userService.save(userData, token);
        }
        return result;
    }

    private boolean isValidUserData(final UserData userData) {
        List<FieldError> fieldErrors = new ArrayList<>();
        if (userData == null || userData.getName() == null) {
            fieldErrors.add(new FieldError("name", "Name should not be empty."));
        } else {
            userData.setName(userData.getName().trim());
            if (StringUtils.isEmpty(userData.getName()) || userData.getName().length() < 2) {
                fieldErrors.add(new FieldError("name", "Min length 2 characters"));
            }
        }
        // terms accepted needed only on first submit as usually, it's asked on first
        // submit and although it's possible to take it back, this possibility can be
        // through email or other channels
        if (userData == null || userData.getId() == null && userData.getTermsAccepted() == null) {
            fieldErrors.add(new FieldError("termsAccepted", "Agree to terms, to continue"));
        } else if (userData.getId() == null && !userData.getTermsAccepted()) {
            fieldErrors.add(new FieldError("termsAccepted", "Agree to terms, to continue"));
        }
        // sectors required but can be empty to remove sectors
        if (userData == null || userData.getUserSectors() == null) {
            fieldErrors.add(new FieldError("sectors", "Sectors are required"));
        }
        if (CollectionUtils.isEmpty(fieldErrors)) {
            return true;
        }
        throw new InvalidParametersException(fieldErrors);
    }

}
