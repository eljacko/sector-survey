package com.eljacko.survey.webapi.dto.sector;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.util.CollectionUtils;

import java.util.Collection;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SectorListResponse {

    private Collection<SectorData> sectors;

    public final String toStringMainData() {
        StringBuilder builder = new StringBuilder();
        builder.append("ProductListResponse [sectors size=");
        if (CollectionUtils.isEmpty(sectors)) {
            builder.append(0);
        } else {
            builder.append(sectors.size());
        }
        builder.append("]");
        return builder.toString();
    }

}
