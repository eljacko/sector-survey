package com.eljacko.survey.webapi.dto.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserDataSaveResult {

    private String token;
    private UserData userData;

    public UserDataSaveResult(final UserData userData, final String token) {
        this.userData = userData;
        this.token = token;
    }
}
