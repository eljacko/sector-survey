package com.eljacko.survey.webapi.util;

import com.eljacko.survey.webapi.constant.Loggers;
import io.undertow.server.handlers.accesslog.AccessLogReceiver;

public class WebApiAccessLogReceiver implements AccessLogReceiver {

    @Override
    public final void logMessage(final String message) {
        if (Loggers.ACCESS_LOG_LOGGER.isInfoEnabled()) {
            Loggers.ACCESS_LOG_LOGGER.info(message);
        }
    }
}
