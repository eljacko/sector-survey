package com.eljacko.survey.webapi;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories("com.eljacko.survey.datamodel.repository")
@EntityScan(basePackages = "com.eljacko.survey.datamodel.entity")
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class JpaConfig {

}
