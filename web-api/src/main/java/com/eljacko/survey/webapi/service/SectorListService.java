package com.eljacko.survey.webapi.service;

import com.eljacko.survey.webapi.dto.sector.SectorData;

import java.util.List;

public interface SectorListService {

    List<SectorData> getSectors(Long categoryId, Integer depth);

    void setSectors(List<SectorData> sectors);

}
