package com.eljacko.survey.webapi.dto.user;

import com.eljacko.survey.datamodel.constant.ValidationMessages;
import com.eljacko.survey.datamodel.dto.UserDto;
import com.eljacko.survey.datamodel.dto.UserSectorDto;
import com.eljacko.survey.datamodel.entity.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class UserData {

    private Long id;
    @NotNull(message = ValidationMessages.NOT_NULL)
    private String name;
    private Boolean termsAccepted;
    private List<UserSectorDto> userSectors;

    public UserData(final User user) {
        this.id = user.getId();
        this.name = user.getName();
        this.termsAccepted = user.getTermsAccepted() != null;
        this.userSectors = user.getUserSectors().stream().map(UserSectorDto::new)
                .collect(Collectors.toList());
    }

    public UserData(final UserDto user) {
        this.id = user.getId();
        this.name = user.getName();
        this.termsAccepted = user.getTermsAccepted() != null;
        this.userSectors = user.getSectors();
    }
}
