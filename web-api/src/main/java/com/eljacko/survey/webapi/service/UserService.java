package com.eljacko.survey.webapi.service;

import com.eljacko.survey.webapi.dto.user.UserData;
import com.eljacko.survey.webapi.dto.user.UserDataSaveResult;

public interface UserService {

    UserDataSaveResult save(UserData userData, String token);

    UserData getUserData(Long userId);

}
