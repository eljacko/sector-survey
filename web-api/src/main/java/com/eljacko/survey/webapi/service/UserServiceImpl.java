package com.eljacko.survey.webapi.service;

import com.eljacko.survey.datamodel.dto.UserDto;
import com.eljacko.survey.datamodel.dto.UserSectorDto;
import com.eljacko.survey.datamodel.entity.User;
import com.eljacko.survey.datamodel.entity.UserSector;
import com.eljacko.survey.datamodel.repository.SectorRepository;
import com.eljacko.survey.datamodel.repository.UserRepository;
import com.eljacko.survey.datamodel.repository.UserSectorRepository;
import com.eljacko.survey.webapi.dto.user.UserData;
import com.eljacko.survey.webapi.dto.user.UserDataSaveResult;
import com.eljacko.survey.webapi.exception.InvalidParameterException;
import com.eljacko.survey.webapi.exception.NotFoundException;
import com.eljacko.survey.webapi.exception.UnauthorizedException;
import com.eljacko.survey.webapi.service.util.DateUtilService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.internal.util.Assert;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class UserServiceImpl implements UserService {

    private static final Integer EDIT_WINDOW_IN_MINUTES = 15;

    private final UserRepository userRepository;
    private final UserSectorRepository userSectorRepository;
    private final SectorRepository sectorRepository;
    private final DateUtilService dateUtilService;

    @Override
    @Transactional(readOnly = true)
    public UserData getUserData(final Long userId) {
        Assert.notNull(userId, "User id must not be null");
        UserDto userDto = userRepository.getUser(userId);
        return new UserData(userDto);
    }

    @Override
    public UserDataSaveResult save(final UserData userData, final String token) {
        Assert.notNull(userData, "User data must not be null");
        log.debug("Save user data id={}", userData.getId());
        if (userData.getId() != null) {
            if (token == null) {
                throw new InvalidParameterException("token", "Token missing");
            }
            if (!userRepository.isUpdateAllowed(userData.getId(), token,
                    dateUtilService.getPastTimestamp(EDIT_WINDOW_IN_MINUTES))) {
                throw new UnauthorizedException();
            }
        }
        User userDb;
        if (userData.getId() == null) {
            userDb = new User();
            userDb.setUserSectors(new HashSet<>());
            userDb.setTermsAccepted(new Timestamp(new java.util.Date().getTime()));
            userDb.setToken(UUID.randomUUID().toString());
        } else {
            userDb = userRepository.findById(userData.getId())
                    .orElseThrow(() -> new NotFoundException());
        }

        userDb.setLastModified(new Timestamp(new java.util.Date().getTime()));
        userDb.setName(userData.getName());
        userRepository.save(userDb);

        Set<UserSector> userSectorsDb = userDb.getUserSectors();
        List<Long> deletedUserSectors = new ArrayList<>();
        List<Long> leftUserSectorsInDb = new ArrayList<>();

        userSectorsDb.forEach(us -> {
            Optional<UserSectorDto> existingUserSectorDto = existsItem(
                    us.getSector().getId(), userData.getUserSectors());
            if (existingUserSectorDto.isPresent()) {
                leftUserSectorsInDb.add(us.getSector().getId());
            } else {
                userSectorRepository.delete(us);
                userSectorsDb.remove(us);
                deletedUserSectors.add(us.getSector().getId());
            }
        });

        if (!deletedUserSectors.isEmpty()) {
            log.debug("Deleted {} sectors for user {}, sectors: {}",
                    deletedUserSectors.size(), userDb.getId(), deletedUserSectors);
        }

        if (!userData.getUserSectors().isEmpty()) {
            List<Long> addedSectors = new ArrayList<>();
            userData.getUserSectors().stream().filter(addedSector -> !leftUserSectorsInDb
                    .contains(addedSector.getSectorId())).forEach(addedSector -> {
                UserSector us = new UserSector(userDb,
                        sectorRepository.getOne(addedSector.getSectorId()));
                userSectorRepository.save(us);
                addedSectors.add(addedSector.getSectorId());
                userSectorsDb.add(us);
            });
            if (!addedSectors.isEmpty()) {
                log.debug("Added {} sectors for user {}, sectors: {}",
                        addedSectors.size(), userData.getId(), addedSectors);
            }
        }

        return new UserDataSaveResult(new UserData(userDb), userDb.getToken());
    }

    private Optional<UserSectorDto> existsItem(final Long sectorId,
                                                     final List<UserSectorDto> selectedSectors) {
        return selectedSectors.stream().filter(us -> us.getSectorId().equals(sectorId))
                .findAny();
    }

}
