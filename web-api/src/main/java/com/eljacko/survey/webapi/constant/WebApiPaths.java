package com.eljacko.survey.webapi.constant;

public final class WebApiPaths {

    public static final String SECTORS = "/sectors";
    public static final String USERS = "/users";
    public static final String USER = "/user/{userId}";


    private WebApiPaths() {
        throw new UnsupportedOperationException(
                "This is a constants class and cannot be instantiated");
    }
}
