package com.eljacko.survey.webapi.dto.sector;

import com.eljacko.survey.datamodel.constant.ValidationMessages;
import com.eljacko.survey.datamodel.dto.SectorDto;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class SectorData {

    private Long id;
    @NotNull(message = ValidationMessages.NOT_NULL)
    private String name;
    private Long categoryId;
    private List<SectorData> sectors;

    public SectorData(final SectorDto sectorDto) {
        this.id = sectorDto.getId();
        this.name = sectorDto.getName();
        this.categoryId = sectorDto.getCategoryId();
    }

}
