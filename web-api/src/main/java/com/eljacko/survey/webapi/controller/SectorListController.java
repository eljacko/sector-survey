package com.eljacko.survey.webapi.controller;

import com.eljacko.survey.webapi.constant.WebApiPaths;
import com.eljacko.survey.webapi.dto.sector.SectorData;
import com.eljacko.survey.webapi.dto.sector.SectorListResponse;
import com.eljacko.survey.webapi.service.SectorListService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/v1")
@SuppressWarnings("checkstyle:designforextension")
public class SectorListController {

    private final SectorListService sectorListService;

    @GetMapping(WebApiPaths.SECTORS)
    public HttpEntity<?> getSectors(
            @RequestParam(value = "categoryId", required = false) final Long categoryId,
            @RequestParam(value = "depth", required = false) final Integer depth) {
        log.debug("Request to get sectors");
        List<SectorData> result = sectorListService.getSectors(categoryId, depth);
        SectorListResponse response = new SectorListResponse();
        response.setSectors(result);
        log.debug("Found top sectors data {}", response.toStringMainData());
        // converting to json using gson to filter out null value objects
        return new ResponseEntity<SectorListResponse>(response, HttpStatus.OK);
    }

    @PostMapping(WebApiPaths.SECTORS)
    public HttpEntity<?> setSectors(
            @RequestBody @Valid final List<SectorData> sectors) {
        log.debug("Request to set sectors {}", sectors);
        sectorListService.setSectors(sectors);
        return new ResponseEntity<>(HttpStatus.OK);
    }

}
