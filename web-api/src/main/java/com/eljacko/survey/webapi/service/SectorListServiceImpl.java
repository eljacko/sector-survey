package com.eljacko.survey.webapi.service;

import com.eljacko.survey.datamodel.dto.SectorDto;
import com.eljacko.survey.datamodel.entity.Sector;
import com.eljacko.survey.datamodel.repository.SectorRepository;
import com.eljacko.survey.webapi.dto.sector.SectorData;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor
@SuppressWarnings({ "checkstyle:DesignForExtension" })
public class SectorListServiceImpl implements SectorListService {

    private final SectorRepository sectorRepository;

    @Override
    @Transactional(readOnly = true)
    public List<SectorData> getSectors(final Long categoryId, final Integer depth) {
        List<SectorDto> sectorsDtoList = sectorRepository.getSectorsTree(categoryId, depth);
        if (!CollectionUtils.isEmpty(sectorsDtoList)) {
            List<SectorData> sectorDataList = sectorsDtoList.stream().map(SectorData::new)
                    .collect(Collectors.toList());
            AtomicInteger roots = new AtomicInteger();
            final List<SectorData> copyList = new ArrayList<>(sectorDataList);
            copyList.forEach(sector -> {
                if (sector.getCategoryId() == null) {
                    roots.set(roots.get() + 1);
                }
                sectorDataList
                        .stream()
                        .filter(category -> category.getId().equals(sector.getCategoryId()))
                        .findAny()
                        .ifPresent(category -> {
                            if (category.getSectors() == null) {
                                category.setSectors(new ArrayList<>());
                            }
                            category.getSectors().add(sector);
                        });
            });
            sectorDataList.subList(roots.get(), sectorDataList.size()).clear();
            return sectorDataList;
        }
        return Collections.<SectorData>emptyList();
    }

    @Override
    @Transactional
    public void setSectors(final List<SectorData> sectors) {
        saveSectors(null, sectors);
    }

    private void saveSectors(final Long id, final List<SectorData> sectors) {
        for (SectorData sectorData : sectors) {
            Sector sectorDto = new Sector();
            sectorDto.setName(sectorData.getName());
            if (id != null) {
                sectorDto.setCategory(sectorRepository.findById(id).get());
            }
            sectorRepository.save(sectorDto);
            if (sectorData.getSectors() != null) {
                saveSectors(sectorDto.getId(), sectorData.getSectors());
            }
        }

    }
}
