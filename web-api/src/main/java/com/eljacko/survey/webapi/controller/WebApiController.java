package com.eljacko.survey.webapi.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/v1/web-api")
public class WebApiController {

    @GetMapping("/ping")
    final HttpEntity<String> ping() {
        final String result = "OK";
        log.info("ping response: {}", result);
        return new ResponseEntity<String>(result, HttpStatus.OK);
    }

}
