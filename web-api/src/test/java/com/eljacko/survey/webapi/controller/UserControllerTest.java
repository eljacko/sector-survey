package com.eljacko.survey.webapi.controller;

import com.eljacko.survey.webapi.constant.ErrorCodes;
import com.eljacko.survey.webapi.dto.user.UserData;
import com.eljacko.survey.webapi.service.JSONService;
import com.eljacko.survey.webapi.service.UserService;
import com.eljacko.survey.webapi.service.util.DateUtilService;
import com.google.gson.Gson;
import lombok.Getter;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;

import java.time.Instant;
import java.util.function.Consumer;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = { UserController.class })
@AutoConfigureWebMvc
@SuppressWarnings({"checkstyle:MethodName", "checkstyle:MagicNumber", "PMD"})
public class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserService userService;
    @MockBean
    private DateUtilService dateUtilService;
    @MockBean
    private JSONService jsonService;

    @Getter
    private static final String ORIGIN = "http://localhost";
    private static final String ENDPOINT = "/v1/users";

    @Test
    public final void add_NoContent_BadRequest() throws Exception {
        when(dateUtilService.getInstantNow()).thenReturn(Instant.parse("2020-10-31T10:12:35Z"));
        mockMvc.perform(post(ENDPOINT)
                .header(org.springframework.http.HttpHeaders.ORIGIN, ORIGIN)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(status().isBadRequest());
    }


    private void verifyAddInvalidData(final UserData userData, final String fieldName)
            throws Exception {
        Mockito.when(jsonService.getGson()).thenReturn(new Gson());
        when(dateUtilService.getInstantNow()).thenReturn(Instant.parse("2020-10-30T10:12:35Z"));
        mockMvc.perform(post(ENDPOINT)
                .header(org.springframework.http.HttpHeaders.ORIGIN, ORIGIN)
                .content(jsonService.toJson(userData))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print()).andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.code", is(ErrorCodes.INVALID_DATA)))
                .andExpect(jsonPath("$.extraData[0].field", is(fieldName)));
    }

    private UserData getUserData(final Consumer<UserData> dataSetter) {
        UserData userData = new UserData();
        if (dataSetter != null) {
            dataSetter.accept(userData);
        }
        return userData;
    }
}
