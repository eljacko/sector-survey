#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" --dbname "$POSTGRES_DB" <<-EOSQL

CREATE USER superuser WITH PASSWORD 'password';
CREATE USER web_api WITH PASSWORD 'password';

CREATE DATABASE survey_integration_tests WITH OWNER superuser
    ENCODING 'UTF8' LC_COLLATE 'C' LC_CTYPE 'C' TEMPLATE template0;

\c survey_integration_tests

ALTER SCHEMA public OWNER TO superuser;

EOSQL
