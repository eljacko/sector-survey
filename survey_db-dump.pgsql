--
-- PostgreSQL database dump
--

-- Dumped from database version 12.3
-- Dumped by pg_dump version 12.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: sectors; Type: TABLE; Schema: public; Owner: superuser
--

CREATE TABLE public.sectors (
    id bigint NOT NULL,
    name character varying(64) NOT NULL,
    category_id bigint
);


ALTER TABLE public.sectors OWNER TO superuser;

--
-- Name: sectors_id_seq; Type: SEQUENCE; Schema: public; Owner: superuser
--

CREATE SEQUENCE public.sectors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sectors_id_seq OWNER TO superuser;

--
-- Name: user_sector; Type: TABLE; Schema: public; Owner: superuser
--

CREATE TABLE public.user_sector (
    id bigint NOT NULL,
    sector_id bigint NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.user_sector OWNER TO superuser;

--
-- Name: user_sector_2; Type: TABLE; Schema: public; Owner: superuser
--

CREATE TABLE public.user_sector_2 (
    id bigint NOT NULL,
    sector_id bigint NOT NULL,
    user_id bigint NOT NULL
);


ALTER TABLE public.user_sector_2 OWNER TO superuser;

--
-- Name: user_sector_id_seq; Type: SEQUENCE; Schema: public; Owner: superuser
--

CREATE SEQUENCE public.user_sector_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_sector_id_seq OWNER TO superuser;

--
-- Name: users; Type: TABLE; Schema: public; Owner: superuser
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    last_modified timestamp without time zone NOT NULL,
    name character varying(255) NOT NULL,
    terms_accepted timestamp without time zone NOT NULL,
    token character varying(255)
);


ALTER TABLE public.users OWNER TO superuser;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: superuser
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO superuser;

--
-- Data for Name: sectors; Type: TABLE DATA; Schema: public; Owner: superuser
--

COPY public.sectors (id, name, category_id) FROM stdin;
1	Manufacturing	\N
2	Construction materials	1
3	Electronics and Optics	1
4	Food and Beverage	1
5	Confectionery product	4
6	Beverages	4
7	Fish & fish products	4
8	Meat & meat products	4
9	Milk & dairy products	4
10	Other	4
11	Sweets &; snack food	4
12	Furniture	1
13	Bathroom/sauna	12
14	Bedroom	12
15	Children’s room	12
16	Kitchen	12
17	Living room	12
18	Office	12
19	Other (Furniture)	12
20	Outdoor	12
21	Project furniture	12
22	Machinery	1
23	Machinery components	22
24	Machinery equipment/tools	22
25	Manufacture of machinery	22
26	Maritime	22
27	Aluminium and steel workboats	26
28	Boat/Yacht building	26
29	Ship repair and conversion	26
30	Metal structures	22
31	Other	22
32	Repair and maintenance service	22
33	Metalworking	1
34	Construction of metal structures	33
35	Houses and buildings	33
36	Metal products	33
37	Metal works	33
38	CNC-machining	37
39	Forgings, Fasteners	37
40	Gas, Plasma, Laser cutting	37
41	MIG, TIG, Aluminum welding	37
42	Plastic and Rubber	1
43	Packaging	42
44	Plastic goods	42
45	Plastic processing technology	42
46	Blowing	45
47	Moulding	45
48	Plastics welding and processing	45
49	Plastic profiles	42
50	Printing	1
51	Advertising	50
52	Book/Periodicals printing	50
53	Labelling and packaging printing	50
54	Textile and Clothing	1
55	Clothing	54
56	Textile	54
57	Wood	1
58	Other (Wood)	57
59	Wooden building materials	57
60	Wooden houses	57
61	Other	\N
62	Creative industries	61
63	Energy technology	61
64	Environment	61
65	Service	\N
66	Business services	65
67	Engineering	65
68	Information Technology and Telecommunications	65
69	Data processing, Web portals, E-marketing	68
70	Programming, Consultancy	68
71	Software, Hardware	68
72	Telecommunications	68
73	Tourism	65
74	Translation services	65
75	Transport and Logistics	65
76	Air	75
77	Rail	75
78	Road	75
79	Water	75
\.


--
-- Data for Name: user_sector; Type: TABLE DATA; Schema: public; Owner: superuser
--

COPY public.user_sector (id, sector_id, user_id) FROM stdin;
\.


--
-- Data for Name: user_sector_2; Type: TABLE DATA; Schema: public; Owner: superuser
--

COPY public.user_sector_2 (id, sector_id, user_id) FROM stdin;
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: superuser
--

COPY public.users (id, last_modified, name, terms_accepted, token) FROM stdin;
\.


--
-- Name: sectors_id_seq; Type: SEQUENCE SET; Schema: public; Owner: superuser
--

SELECT pg_catalog.setval('public.sectors_id_seq', 79, true);


--
-- Name: user_sector_id_seq; Type: SEQUENCE SET; Schema: public; Owner: superuser
--

SELECT pg_catalog.setval('public.user_sector_id_seq', 1, false);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: superuser
--

SELECT pg_catalog.setval('public.users_id_seq', 1, false);


--
-- Name: sectors sectors_pkey; Type: CONSTRAINT; Schema: public; Owner: superuser
--

ALTER TABLE ONLY public.sectors
    ADD CONSTRAINT sectors_pkey PRIMARY KEY (id);


--
-- Name: user_sector_2 user_sector_2_pkey; Type: CONSTRAINT; Schema: public; Owner: superuser
--

ALTER TABLE ONLY public.user_sector_2
    ADD CONSTRAINT user_sector_2_pkey PRIMARY KEY (id);


--
-- Name: user_sector user_sector_pkey; Type: CONSTRAINT; Schema: public; Owner: superuser
--

ALTER TABLE ONLY public.user_sector
    ADD CONSTRAINT user_sector_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: superuser
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: sectors fk1605tmy20iw5sl7710bb79qo4; Type: FK CONSTRAINT; Schema: public; Owner: superuser
--

ALTER TABLE ONLY public.sectors
    ADD CONSTRAINT fk1605tmy20iw5sl7710bb79qo4 FOREIGN KEY (category_id) REFERENCES public.sectors(id);


--
-- Name: user_sector fk2ushnhnnpqe3p9bx45dapookg; Type: FK CONSTRAINT; Schema: public; Owner: superuser
--

ALTER TABLE ONLY public.user_sector
    ADD CONSTRAINT fk2ushnhnnpqe3p9bx45dapookg FOREIGN KEY (sector_id) REFERENCES public.sectors(id);


--
-- Name: sectors fki4l0wmdg1hr7i5lcfk1xwx8ko; Type: FK CONSTRAINT; Schema: public; Owner: superuser
--

ALTER TABLE ONLY public.sectors
    ADD CONSTRAINT fki4l0wmdg1hr7i5lcfk1xwx8ko FOREIGN KEY (id) REFERENCES public.sectors(id);


--
-- Name: user_sector fki9oqdki04n282fitqc08faowr; Type: FK CONSTRAINT; Schema: public; Owner: superuser
--

ALTER TABLE ONLY public.user_sector
    ADD CONSTRAINT fki9oqdki04n282fitqc08faowr FOREIGN KEY (user_id) REFERENCES public.users(id);


--
-- PostgreSQL database dump complete
--

